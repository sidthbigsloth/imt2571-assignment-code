<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            $this->db = new PDO('mysql:host=localhost;dbname=test;charset=utf8mb4');
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
		$table_name = 'book'; 
		$test = "SELECT 1 FROM " . $table_name . " LIMIT 1";
		$test = $this->db->query($test); 

		if($test)
		{
			foreach($this->db->query('SELECT * FROM book') as $row) 
			{
				$booklist[] = new book($row['Title'], $row['Author'], $row['Description'], $row['Id']);
			}
		}
		else
		{
			$error = 'could not find table';
			throw new Exception ($error);
		}

        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$book = null;
		$table_name = 'book'; 
		$test = "SELECT 1 FROM " . $table_name . " LIMIT 1";
		$test = $this->db->query($test); 
		if($test)
		{
			if(is_numeric($id))
			{
				if($id>=0)
				{
					$stmt = $this->db->prepare("SELECT * FROM book WHERE Id=?");
					$stmt->execute(array($id));
					$row = $stmt->fetch(PDO::FETCH_ASSOC);
					$book = new book($row['Title'], $row['Author'], $row['Description'], $row['Id']);
					if($book->title == "" || $book->author == "")
					{
						return NULL;
					}
				}
				else
				{
					return NULL;
				}
			}
			else
			{
				$error = 'invalid ID';
				throw new Exception ($error);
			}
		}
		else
		{
			$error = 'could not find table';
			throw new Exception ($error);
		}
		
		
        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		if($book->title != "" && $book->author != "")
		{
			$stmt = $this->db->prepare("INSERT INTO book(Title, Author, Description) VALUES(?, ?, ?)");
			$stmt->execute(array($book->title, $book->author, $book->description));
			$book->id = $this->db->lastInsertId();
		}
		else
		{
			$error = 'title and author can not be empty';
			throw new Exception ($error);
		}
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		if($book->title != "" && $book->author != "")
		{
			$stmt = $this->db->prepare("UPDATE book SET Title = ?, Author = ?, Description = ? WHERE Id = ?");
			$stmt->execute(array($book->title, $book->author, $book->description, $book->id));
		}
		else
		{
			$error = 'title and author can not be empty';
			throw new Exception ($error);
		}
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		$stmt = $this->db->prepare("DELETE FROM book WHERE id = ?");
		$stmt->execute(array($id));
    }
	
}

?>